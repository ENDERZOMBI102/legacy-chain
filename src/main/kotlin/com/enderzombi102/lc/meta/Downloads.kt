package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader

class Downloads( reader: JsonReader ) {
	lateinit var client: Download
	var clientMappings: Download? = null
	var server: Download? = null
	var serverMappings: Download? = null

	init {
		reader.beginObject { name ->
			when ( name ) {
				"client" -> client = Download( this )
				"client_mappings" -> clientMappings = Download( this )
				"server" -> server = Download( this )
				"server_mappings" -> serverMappings = Download( this )
			}
		}
	}
}