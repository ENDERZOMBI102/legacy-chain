package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader
import java.net.URL
import kotlin.properties.Delegates.notNull

class AssetIndex(reader: JsonReader) {
	lateinit var id: String
		private set
	lateinit var sha1: String
		private set
	var size: Int by notNull()
		private set
	var totalSize: Int by notNull()
		private set
	lateinit var url: URL
		private set

	init {
		reader.beginObject { name ->
			when ( name ) {
				"id" -> id = nextString()
				"sha1" -> sha1 = nextString()
				"size" -> size = nextInt()
				"totalSize" -> totalSize = nextInt()
				"url" -> url = URL( nextString() )
			}
		}
	}
}
