package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader
import java.io.InputStreamReader
import java.net.URL

object Manifest {
	val latestRelease: ManifestEntry
	val latestSnapshot: ManifestEntry
	val versions: List<ManifestEntry>

	private const val url = "https://piston-meta.mojang.com/mc/game/version_manifest_v2.json"

	init {
		val reader = JsonReader.json( InputStreamReader( URL( url ).openStream() ) )
		var release: ManifestEntry? = null
		var snapshot: ManifestEntry? = null
		var latestReleaseName: String? = null
		var latestSnapshotName: String? = null

		versions = buildList {
			reader.beginObject()
			while ( reader.hasNext() ) when ( reader.nextName() ) {
				"latest" -> reader.beginObject { name ->
					when ( name ) {
						"release" -> latestReleaseName = nextString()
						"snapshot" -> latestSnapshotName = nextString()
					}
				}
				"versions" -> reader.beginArray {
					val meta = ManifestEntry.parse(reader) ?: return@beginArray println( "Warn: invalid version index entry at ${reader.path()}" )

					if ( meta.id == latestReleaseName )
						release = meta
					if ( meta.id == latestSnapshotName )
						snapshot = meta

					add(meta)
				}
			}
			reader.endObject()
		}
		reader.close()

		latestRelease = release!!
		latestSnapshot = snapshot!!
	}

	operator fun get( id: String ): ManifestEntry =
		versions.first { it.id == id }
}
