package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.beginObject
import org.quiltmc.json5.JsonReader

class Logging( reader: JsonReader ) {
	lateinit var client: LoggingMeta
		private set

	init {
		reader.beginObject { name ->
			when ( name ) {
				"client" -> client = LoggingMeta( this )
				else -> skipValue()
			}
		}
	}

	class LoggingMeta( reader: JsonReader ) {
		lateinit var argument: String
			private set
		lateinit var file: Pair<String, Download>
			private set
		lateinit var type: String
			private set

		init {
			reader.beginObject { name ->
				when ( name ) {
					"argument" -> argument = nextString()
					"file" -> {
						var id: String? = null
						var download: Download? = null
						beginObject { name ->
							when ( name ) {
								"id" -> id = nextString()
								else -> download = Download( this )
							}
						}
						file = id!! to download!!
					}
					"type" -> type = nextString()
				}
			}
		}
	}
}