package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader
import kotlin.properties.Delegates.notNull

class JavaVersion( reader: JsonReader ) {
	lateinit var component: Component
		private set
	var majorVersion: Int by notNull()
		private set

	init {
		reader.beginObject { name ->
			when ( name ) {
				"component" -> component = ofCode( nextString() )
				"majorVersion" -> majorVersion = nextInt()
			}
		}
	}

	enum class Component( override val code: String ) : Stringed<Component> {
		Legacy( "jre-legacy" ),
		Gamma( "java-runtime-gamma" )
	}
}
