package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader
import java.io.InputStreamReader
import java.net.URL
import java.util.*

data class ManifestEntry( val id: String, val type: Type, val url: URL, val time: Date, val releaseTime: Date, val sha1: String ) {
	val version: Lazy<Version> = lazy {
		JsonReader.json( InputStreamReader( url.openStream() ) ).use( ::Version )
	}

	companion object {
		fun parse( reader: JsonReader ): ManifestEntry? {
			reader.beginObject()

			var id: String? = null
			var type: String? = null
			var url: String? = null
			var time: String? = null
			var releaseTime: String? = null
			var sha1: String? = null

			while ( reader.hasNext() ) when ( reader.nextName() ) {
				"id" -> id = reader.nextString()
				"type" -> type = reader.nextString()
				"url" -> url = reader.nextString()
				"time" -> time = reader.nextString()
				"releaseTime" -> releaseTime = reader.nextString()
				"sha1" -> sha1 = reader.nextString()
				else -> reader.skipValue()
			}

			reader.endObject()

			if ( id == null || type == null || url == null || time == null || releaseTime == null || sha1 == null )
				return null

			return ManifestEntry(
				id,
				ofCode( type ),
				URL( url ),
				dateFormat.parse( time ),
				dateFormat.parse( releaseTime ),
				sha1
			)
		}
	}
}