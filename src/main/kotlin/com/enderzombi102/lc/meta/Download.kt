package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader
import org.quiltmc.json5.JsonToken
import java.net.URL
import kotlin.properties.Delegates.notNull

class Download( reader: JsonReader ) {
	lateinit var sha1: String
		private set
	var size: Int by notNull()
		private set
	lateinit var url: URL
		private set

	init {
		val setter: JsonReader.(String) -> Unit = { name ->
			when ( name ) {
				"sha1" -> sha1 = nextString()
				"size" -> size = nextInt()
				"url" -> url = URL( nextString() )
			}
		}

		if ( reader.peek() == JsonToken.BEGIN_OBJECT )
			reader.beginObject( setter )
		else
			while ( reader.hasNext() )
				setter.invoke( reader, reader.nextName() )
	}
}
