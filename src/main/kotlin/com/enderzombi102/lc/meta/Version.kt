package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.*
import org.quiltmc.json5.JsonReader
import java.util.*
import kotlin.properties.Delegates.notNull

class Version( reader: JsonReader ) {
	lateinit var assetIndex: AssetIndex
		private set
	lateinit var assets: String
		private set
	lateinit var downloads: Downloads
		private set
	lateinit var id: String
		private set
	lateinit var javaVersion: JavaVersion
		private set
	lateinit var libraries: List<Library>
		private set
	var logging: Logging? = null
		private set
	lateinit var mainClass: String
		private set
	var minecraftArguments: String? = null
		private set
	var minimumLauncherVersion: Int by notNull()
		private set
	lateinit var releaseTime: Date
		private set
	lateinit var time: Date
		private set
	lateinit var type: Type
		private set
	init {
		reader.beginObject { name ->
			when ( name ) {
				"arguments" -> skipValue()
				"assetIndex" -> assetIndex = AssetIndex( this )
				"assets" -> assets = nextString()
				"complianceLevel" -> skipValue()
				"downloads" -> downloads = Downloads( this )
				"id" -> id = nextString()
				"javaVersion" -> javaVersion = JavaVersion( this )
				"libraries" -> libraries = Library.parse(this)
				"logging" -> logging = Logging( this )
				"mainClass" -> mainClass = nextString()
				"minecraftArguments" -> minecraftArguments = nextString()
				"minimumLauncherVersion" -> minimumLauncherVersion = nextInt()
				"releaseTime" -> releaseTime = dateFormat.parse( nextString() )
				"time" -> time = dateFormat.parse( nextString() )
				"type" -> type = ofCode( nextString() )
			}
		}
	}
}