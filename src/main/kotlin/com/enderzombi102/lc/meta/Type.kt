package com.enderzombi102.lc.meta

import com.enderzombi102.lc.util.Stringed

enum class Type( override val code: String ) : Stringed<Type> {
	Release("release"),
	Snapshot("snapshot"),
	Beta("old_beta"),
	Alpha("old_alpha")
}