package com.enderzombi102.lc

import joptsimple.OptionParser
import java.io.File

fun main( argv: Array<String> ) {
	val parser = OptionParser()

	parser.accepts( "mcp", "The path where MCP configs will reside." )
		.withRequiredArg()
		.ofType( File::class.java )
		.defaultsTo( File("./mcp") )

	val options = parser.parse( *argv )

	downloadMcp( options.valueOf( "mcp" ) as File )
}

fun downloadMcp( folder: File ) {
	if ( folder.resolve( "runtime/commands.py" ).exists() )
		return println( "skipping MCP download" )

	if ( folder.exists() ) {
		println( "a MCP folder was found, but no valid installation resides in it, will delete and download again." )
		if (! folder.deleteRecursively() )
			error( "failed to delete $folder, please, manually delete and retry." )
	}



}