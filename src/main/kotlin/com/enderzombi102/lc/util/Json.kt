@file:OptIn(ExperimentalContracts::class)
package com.enderzombi102.lc.util

import org.quiltmc.json5.JsonReader
import java.text.SimpleDateFormat
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

fun JsonReader.beginObject(body: JsonReader.(name: String ) -> Unit ) {
	contract { callsInPlace( body, InvocationKind.UNKNOWN ) }
	beginObject()
	while ( hasNext() )
		body.invoke( this, nextName() )
	endObject()
}

fun JsonReader.beginArray(body: JsonReader.() -> Unit ) {
	contract { callsInPlace( body, InvocationKind.UNKNOWN ) }
	beginArray()
	while ( hasNext() )
		body.invoke( this )
	endArray()
}
