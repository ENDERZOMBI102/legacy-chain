package com.enderzombi102.lc.util

interface Stringed<T> where T : Stringed<T>, T : Enum<T> {
	val code: String
}

inline fun <reified T> ofCode(code: String ): T where T : Enum<T>, T : Stringed<T> =
	enumValues<T>().first { it.code == code }
