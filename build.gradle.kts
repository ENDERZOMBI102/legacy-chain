plugins {
    kotlin( "jvm" ) version "1.7.21"
}

group = "com.enderzombi102"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
	maven( "https://repo.sleeping.town" )
	maven( "https://maven.quiltmc.org/repository/release" )
}

dependencies {
    testImplementation( kotlin( "test" ) )
	implementation( "blue.endless:jankson:1.2.1" )
	implementation( "com.unascribed:flexver-java:1.0.2" )
	implementation( "net.sf.jopt-simple:jopt-simple:6.0-alpha-3" )
	implementation( "net.sf.jopt-simple:jopt-simple:6.0-alpha-3" )
	implementation( "org.quiltmc:quilt-json5:1.0.2" )
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}